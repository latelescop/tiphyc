#!/usr/bin/env python3
import os
import sys
from shutil import copytree
from pathlib import Path
from functools import partial

from mapscan_tools.utils import get_tiffs
from mapscan_tools.classification import (
    greens_remap_labels,
    greens_segmentation,
    greens_classification,
)
from mapscan_tools.workflow import preprocessing, get_jobs, blue_processing

# Needed in order to launch a pure CLI QGIS instance
os.environ["QT_QPA_PLATFORM"] = "offscreen"

INDIR = "/home/vidlb/Projets/tiphyc/georef_utm/"
WORKSPACE = "/home/vidlb/Projets/tiphyc/processing"
JOBS = 8
# BATCH_LIST = ['Ansongo',]
# BATCH_LIST = ['Hombori', 'InTillit', 'Dori', 'Tera', 'Niamey', 'Ouallam', 'Kaya', 'Pissila', 'Sebba', 'Gotheye', 'Boulsa', 'FadaNgourma']
# BATCH_LIST = ['Natitingou', 'Bimbereke', 'Djougou', 'Parakou', 'Sokode', 'Abomey', 'Save', 'Zagnanado']
BATCH_LIST = [
    "Arli",
    "BambaraMaounde",
    "Bandiagara",
    "Dagana",
    "Diapaga",
    "Douentza",
    "Dunkassa",
    "Gao",
    "Gaya",
    "Gourma-Rharous",
    "Kandi",
    "Kirtachi",
    "Linguere",
    "Louga",
    "Ouahigouya",
    "Pama",
    "Saint-Louis",
    "Thies",
    "Tillabery",
]


# Green segmentation parameters, may vary from one map to another
GAUSS_SIGMA = 7
DEPTH = 0.02
FLOOD = 0.1
MIN_SEGMENT_SIZE = 500
GMM_FIELDS = [
    # "mean_0",
    # "std_0",
    "mean_1",
    # "min_1",
    # "max_1",
    # "mean_2",
    "mean_3",
    # "mean_4",
    # "mean_5",
    # "mean_6",
]


def main(indir=INDIR, outdir=WORKSPACE, jobs=JOBS):
    """Process images in parallel"""

    files = get_tiffs(indir, fullpath=True)
    files = list(
        filter(lambda s: os.path.basename(s).split("_")[0] in BATCH_LIST, files)
    )

    indir = os.path.abspath(indir)
    outdir = os.path.abspath(outdir)

    if not os.path.exists(outdir):
        copytree("template_dir", outdir)
        os.chdir(outdir)
        get_jobs(preprocessing, files, jobs)
    else:
        os.chdir(outdir)
        get_jobs(preprocessing, files, jobs)

    ### Greens processing - OTB Segmentation
    directories = sorted([str(d.absolute()) for d in Path().iterdir() if d.is_dir()])
    directories = list(filter(lambda d: d.split("/")[-1] in BATCH_LIST, directories))
    # Gaussian blur, watershed, segmentation : already in parallel with OTB
    get_jobs(
        partial(
            greens_segmentation, gauss_sigma=GAUSS_SIGMA, depth=DEPTH, flood=FLOOD, minsize=MIN_SEGMENT_SIZE
        ),
        directories,
        jobs=1,
    )
    # Remap segmentation labels using numba (no parallel option))
    get_jobs(greens_remap_labels, directories, jobs=jobs)

    ### Green features GMM clustering :  Python sklearn
    # Test components : try many classifications and compute AIC / BIC
    # get_jobs(
    #    partial(
    #        greens_classification,
    #        classes=None,
    #        test_components=10,
    #        fields=GMM_FIELDS,
    #        seed=42,
    #    ),
    #    directories,
    #    jobs=1,
    # )

    # get_jobs(
    #    partial(greens_classification, classes=2, fields=["mean_1"], seed=42),
    #    directories,
    #    jobs=jobs,
    # )
    # get_jobs(
    #    partial(greens_classification, classes=2, fields=GMM_FIELDS, seed=42),
    #    directories,
    #    jobs=jobs,
    # )
    get_jobs(
       partial(greens_classification, classes=4, fields=GMM_FIELDS, seed=42),
       directories,
       jobs=jobs,
    )
    # get_jobs(
    #    partial(greens_classification, classes=5, fields=GMM_FIELDS, seed=42),
    #    directories,
    #    jobs=jobs,
    # )
    # get_jobs(
    #     partial(greens_classification, classes=5, fields=["mean_1", "mean_3", "mean_6"], seed=42),
    #     directories,
    #     jobs=jobs,
    # )

    ### Blue raster to vector polygons : Python skimage
    get_jobs(partial(blue_processing), directories, jobs=1)
    ### Blue raster to vector lines : QGIS / GRASS
    directories = [d + "/blue_features" for d in directories]
    from mapscan_tools.qgis_tools import extract_blue_lines

    get_jobs(extract_blue_lines, directories, jobs=JOBS)

    return 0


if __name__ == "__main__":
    sys.exit(main())
