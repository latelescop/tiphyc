#!/usr/bin/env python3
import sys
from pathlib import Path

import numpy as np
import rasterio
from scipy.interpolate import NearestNDInterpolator


def nn_interpolate(data: np.ndarray, nodata: int = 255) -> np.ndarray:
    mask = np.where(~(data==nodata))
    interp = NearestNDInterpolator(np.transpose(mask), data[mask])
    filled_data = interp(*np.indices(data.shape))
    return filled_data


if __name__ == "__main__":
    path = Path(sys.argv[1])
    img = rasterio.open(path)
    profile = img.profile
    data = img.read(1)
    filled = nn_interpolate(data, 255)
    with rasterio.open(Path(sys.argv[2]), 'w', **profile) as out:
       out.write_band(1, filled)
