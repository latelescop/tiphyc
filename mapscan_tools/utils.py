import os
import sys
import subprocess


def find_otb():
    """Retourne l'emplacement de OTB sur la machine"""

    otbpy, otblib = None, None

    if sys.platform == "linux":
        otbroot = "/usr/lib/x86_64-linux-gnu/otb"
        if os.path.exists(otbroot):
            sys.path.append(otbroot + "/lib/otb/python")
            os.environ["OTB_APPLICATION_PATH"] = otbroot + "/otb/applications"
            return True
        otbroot = "/opt/otb"
        if os.path.exists(otbroot):
            sys.path.append(otbroot + "/lib/otb/python")
            os.environ["OTB_APPLICATION_PATH"] = otbroot + "/otb/applications"
            return True
        res = None
        user = os.environ["USER"]
        exp = r".*/OTB-[0-9]\.[0-9]\.[0-9]-Linux64"
        findcmd = "find /home/{} -regex '{}' -type d".format(user, exp)
        res = subprocess.check_output(findcmd, shell=True)[:-1].decode()
        if res:
            sys.path.append(res + "/lib/otb/python")
            os.environ["OTB_APPLICATION_PATH"] = res + "/lib/otb/application"
            return True
    return False


def get_tiffs(directory: str, fullpath=False):
    """Get every tif files in a directory"""
    res = sorted(list(filter(lambda e: e.endswith(".tif"), os.listdir(directory))))
    if fullpath:
        return [directory + f for f in res]
    return res
