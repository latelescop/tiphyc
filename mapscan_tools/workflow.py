import os
from multiprocessing import Pool
from shutil import rmtree
from pathlib import Path

import numpy as np
import rasterio.io
from skimage import exposure, filters
from rasterio.plot import reshape_as_image, reshape_as_raster

from .classification import greens_segmentation
from .raster_tools import (
    extract_canvas,
    untangle_colors,
    mask_array,
    clean_bin_images,
    blue_hydro_network,
    blue_water_surface,
    blue_hydro_zonal,
    write_color_components,
)


def preprocessing(
    img_path: str,
    adjust: bool = True,
    gaussian: bool = False,
    binary_cleaning: bool = True,
    write_output: bool = True,
    write_rgb_hsv: bool = False,
    verbose: bool = False,
):
    """Clip image, adjust sigmoid, gaussian filter and colors extraction"""

    if verbose:
        print("Processing " + os.path.basename(img_path))
    mapname = os.path.basename(img_path).split("_")[0]
    if not os.path.exists(os.path.basename(mapname)):
        os.mkdir(mapname)

    out_img, out_mask, out_meta = extract_canvas(img_path)
    # Required shape for skimage
    out_img = reshape_as_image(out_img)
    # Use compression for raster outputs
    out_meta.update({"compress": "deflate", "predictor": 2})

    if adjust:
        if verbose:
            print("Adjust sigmoid...")
        out_img = exposure.adjust_sigmoid(out_img, cutoff=0.7, gain=12)
    if gaussian:
        if verbose:
            print("Applying gaussian filter...")
        out_img = filters.gaussian(
            out_img, sigma=0.5, multichannel=True, preserve_range=True
        )

    # Rescale in [0, 1]
    out_img = out_img / out_img.max()

    if verbose:
        print("Extracting colors...")
    r, g, b, wh, bl = untangle_colors(out_img)
    color_bin = {"reds": r, "greens": g, "blues": b, "whites": wh, "blacks": bl}

    # Rescale pixel values in range [1, 255] in order to use 0 as nodata
    out_img = (out_img * 254 + 1).astype(np.uint8)
    out_img = reshape_as_raster(out_img)
    if not out_img.min() == 1:
        if verbose:
            print("! Min value = " + str(out_img.min()))

    if wh.mean() < 0.5:
        if verbose:
            print(f"! Whites ratio: {wh.mean()}")
    if write_rgb_hsv:
        if verbose:
            print("Export rgb and hsv images")
        write_color_components(out_img, out_meta, outdir=mapname + "/")

    if write_output:
        out_file = mapname + "/_preprocessed_img.tif"
        #os.system(f"ln -s {out_file} {os.path.basename(img_path)}")
        with rasterio.open(out_file, "w", nodata=0, **out_meta) as dest:
            dest.write(mask_array(out_img, out_mask))

        if verbose:
            print("Writing files...")
        out_meta.update({"count": 1})
        for key, array in color_bin.items():
            nodata = None
            out_file = f"{mapname}/{key}.tif"
            with rasterio.open(out_file, "w", nodata=nodata, **out_meta) as dest:
                dest.write_band(1, mask_array(array, out_mask[0]).astype(np.uint8))

    if binary_cleaning:
        if verbose:
            print("Binary cleaning...")
        color_bin = clean_bin_images(color_bin)
        if write_output:
            if verbose:
                print("Writing files...")
            for key, array in color_bin.items():
                nodata = 0 if key not in ("whites", "blacks") else None
                out_file = f"{mapname}/{key}_cleaned.tif"
                with rasterio.open(out_file, "w", nodata=nodata, **out_meta) as dest:
                    dest.write_band(1, mask_array(array, out_mask[0]).astype(np.uint8))

    return out_img, out_meta, color_bin


def blue_processing(indir, overwrite=False):
    # Process blue lines and polygons from raster to vector
    outdir = str(Path(indir) / "blue_features")
    if not os.path.exists("outdir"):
        os.mkdir(outdir)
    else:
        if overwrite:
            rmtree(outdir, ignore_errors=True)
            os.mkdir(outdir)

    os.chdir(outdir)
    img_str = str(Path(indir) / 'blues_cleaned.tif')
    #os.system(f"gdal_sieve.py -st 8 -8 -nomask {} blues_sieved.tif")
    #img = rasterio.open("blues_sieved.tif")
    img = rasterio.open(img_str)
    out_meta = img.meta.copy()
    out_meta.update({"dtype": "float32", "compress": "deflate", "predictor": 2})
    if "nodata" in out_meta.keys():
        out_meta.pop("nodata")

    # Lines
    blues = img.read(1).astype(np.bool)
    blue_meij, blue_lines = blue_hydro_network(blues)
    out_file = f"blues_meijering.tif"
    with rasterio.open(out_file, "w", nodata=np.nan, **out_meta) as dest:
        dest.write_band(1, blue_meij.astype(np.float32))

    out_meta.update({"dtype": "uint8"})
    out_file = f"blues_thinned.tif"
    with rasterio.open(out_file, "w", nodata=0, **out_meta) as dest:
        dest.write_band(1, blue_lines.astype(np.uint8))

    # Polygons
    water = blue_water_surface(blues)
    out_file = "blue_water_surface.tif"
    with rasterio.open(out_file, "w", nodata=0, **out_meta) as dest:
        dest.write_band(1, water.astype(np.uint8))
    # Using Windows yo may need to write full path for gdal script
    os.system(f'gdal_polygonize.py -q blue_water_surface.tif blue_water_surface.gpkg')

    img_str = str(Path(indir) / 'blues.tif')
    blues_full = rasterio.open(img_str).read(1)
    zonal = blue_hydro_zonal(blues_full)
    # Remove plain water from zonal layer
    zonal = zonal * np.logical_not(water)
    out_file = "blue_zonal.tif"
    with rasterio.open(out_file, "w", nodata=0, **out_meta) as dest:
        dest.write_band(1, zonal.astype(np.uint8))

    cleaned_lines = blue_lines * np.logical_not(zonal)
    os.system(f'gdal_polygonize.py -q blue_zonal.tif blue_zonal.gpkg')
    out_file = "blues_thinned_cleaned.tif"
    with rasterio.open(out_file, "w", nodata=0, **out_meta) as dest:
        dest.write_band(1, cleaned_lines.astype(np.uint8))

    return blue_meij, blue_lines, zonal, cleaned_lines


def get_jobs(func, iterable, jobs=1):
    # Single or parallel task launcher from iterable
    if jobs == 1:
        for e in iterable:
            func(e)
    else:
        pool = Pool(jobs)
        for e in iterable:
            pool.apply_async(func, args=(e,))
        pool.close()
        pool.join()
        pool.terminate()

    return True
