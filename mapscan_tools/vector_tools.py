from math import pi, sqrt


def s_compactness(geom):  # Schwartzberg
    p = geom.length
    a = geom.area
    return 1 / (p / (2 * pi * sqrt(a / pi)))


def convex_hull_score(geom):
    return geom.area / geom.convex_hull.area
