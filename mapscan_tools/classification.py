import os
from functools import partial
from pathlib import Path
from shutil import rmtree

import numpy as np
import pandas as pd
import geopandas as gpd
import sklearn
from sklearn.mixture import GaussianMixture, BayesianGaussianMixture

import rasterio
from skimage.measure import regionprops_table
import matplotlib.pyplot as plt

#from .vector_tools import convex_hull_score, s_compactness
from .raster_tools import numba_remap_labels, write_greens_edt

try:
    import otbApplication as otb
except ImportError:
    from .utils import find_otb
    find_otb()
finally:
    import otbApplication as otb


def greens_segmentation(
    indir: str,
    gauss_sigma: int,
    depth: float,
    flood: float,
    minsize: int,
    haralick_radius: int = 10,
    overwrite=False,
) -> None:
    """Gaussian blur and watershed segmentation + stack gauss and haralick indices"""

    infile = str(Path(indir) / "greens_cleaned.tif")
    outdir = str(Path(indir) / "green_features")
    if overwrite:
        rmtree(outdir)
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    os.chdir(outdir)

    if not os.path.exists("greens_edt.tif") or overwrite:
        write_greens_edt(infile)

    gauss_file = f"greens_gaussian_{gauss_sigma}.tif"
    stack_file = "greens_stack_stats.tif"
    if not os.path.exists(stack_file) or overwrite:
        # Gauss 1
        Smooth = otb.Registry.CreateApplication("Smoothing")
        Smooth.SetParameterString("type", "gaussian")
        Smooth.SetParameterFloat("type.gaussian.stdev", 1)
        Smooth.SetParameterString("in", infile)
        Smooth.Execute()

        BandMath = otb.Registry.CreateApplication("BandMathX")
        BandMath.SetParameterString("exp", "im1b1 / im1b1Maxi")
        BandMath.AddImageToParameterInputImageList(
            "il", Smooth.GetParameterOutputImage("out")
        )
        BandMath.SetParameterString("out", "greens_gaussian_1.tif")
        BandMath.ExecuteAndWriteOutput()

        # Gauss with high sigma using dilated green pixels base, input for segmentation,
        BinaryMorpho = otb.Registry.CreateApplication("BinaryMorphologicalOperation")
        BinaryMorpho.SetParameterString("in", infile)
        BinaryMorpho.SetParameterInt("channel", 1)
        BinaryMorpho.SetParameterInt("xradius", 1)
        BinaryMorpho.SetParameterInt("yradius", 1)
        BinaryMorpho.SetParameterString("structype", "cross")
        BinaryMorpho.SetParameterString("filter", "dilate")
        BinaryMorpho.SetParameterOutputImagePixelType("out", otb.ImagePixelType_uint8)
        BinaryMorpho.Execute()

        Smooth = otb.Registry.CreateApplication("Smoothing")
        Smooth.SetParameterString("type", "gaussian")
        Smooth.SetParameterFloat("type.gaussian.stdev", gauss_sigma)
        Smooth.SetParameterInputImage("in", BinaryMorpho.GetParameterOutputImage("out"))
        Smooth.Execute()

        BandMath = otb.Registry.CreateApplication("BandMathX")
        BandMath.SetParameterString("exp", "im1b1 / im1b1Maxi")
        BandMath.AddImageToParameterInputImageList(
            "il", Smooth.GetParameterOutputImage("out")
        )
        BandMath.SetParameterString("out", gauss_file)
        BandMath.ExecuteAndWriteOutput()

        # Stack gaussian + euclidian distance + haralick simple bands (1,2,4,5,8)
        Textures = otb.Registry.CreateApplication("HaralickTextureExtraction")
        Textures.SetParameterString("in", "greens_gaussian_1.tif")
        Textures.SetParameterInt("channel", 1)
        Textures.SetParameterInt("parameters.xrad", haralick_radius)
        Textures.SetParameterInt("parameters.yrad", haralick_radius)
        Textures.SetParameterFloat("parameters.max", 1.0)
        Textures.SetParameterString("texture", "simple")
        Textures.Execute()

        BandMath = otb.Registry.CreateApplication("BandMathX")
        BandMath.AddImageToParameterInputImageList(
            "il", Textures.GetParameterOutputImage("out")
        )
        BandMath.SetParameterString("exp", "{im1b1, im1b2, im1b4, im1b5, im1b8}")
        BandMath.Execute()

        Concat = otb.Registry.CreateApplication("ConcatenateImages")
        Concat.SetParameterStringList(
            "il",
            ["greens_gaussian_1.tif", f"greens_gaussian_{gauss_sigma}.tif"],
        )
        Concat.AddImageToParameterInputImageList(
            "il", BandMath.GetParameterOutputImage("out")
        )
        Concat.SetParameterOutputImagePixelType("out", otb.ImagePixelType_float)
        Concat.SetParameterString(
            "out",
            stack_file + "?gdal:co:COMPRESS=ZSTD&gdal:co:PREDICTOR=3" + "&nodata=nan",
        )
        Concat.ExecuteAndWriteOutput()

        os.remove("greens_gaussian_1.tif")
        os.remove(gauss_file)

    segmentation_file = f"greens_gauss_{gauss_sigma}_watershed_{depth}_{flood}.tif"
    if not os.path.exists(segmentation_file) or overwrite:
        # Watershed segmentation
        Segmentation = otb.Registry.CreateApplication("Segmentation")
        if os.path.exists(gauss_file):
            Segmentation.SetParameterString("in", gauss_file)
        elif os.path.exists(stack_file):
            Segmentation.SetParameterString("in", stack_file + "?bands=2")
        else:
            raise Exception(f"Missing {gauss_file} or {stack_file}")

        Segmentation.SetParameterString("mode", "raster")
        Segmentation.SetParameterFloat("filter.watershed.threshold", depth)
        Segmentation.SetParameterFloat("filter.watershed.level", flood)
        Segmentation.SetParameterString("filter", "watershed")
        Segmentation.Execute()

        # Mask segmentation output
        BandMathX = otb.Registry.CreateApplication("BandMathX")
        BandMathX.SetParameterStringList("il", ["../_preprocessed_img.tif"])
        BandMathX.AddImageToParameterInputImageList(
            "il", Segmentation.GetParameterOutputImage("mode.raster.out")
        )
        BandMathX.SetParameterString(
            "exp",
            "im1b1 == 0 && im1b2 == 0 && im1b3 == 0 ? 0 : (im2b1 == 0 ? im2b1Maxi + 1 : im2b1)",
        )
        BandMathX.SetParameterOutputImagePixelType("out", otb.ImagePixelType_uint32)
        BandMathX.SetParameterString(
            "out",
            segmentation_file
            + "?gdal:co:COMPRESS=DEFLATE&gdal:co:PREDICTOR=2"
            + "&nodata=0",
        )
        BandMathX.ExecuteAndWriteOutput()

        # Link generic filename to latest segmentation file
        os.system(
            f"rm -f greens_watershed.tif && ln -s {segmentation_file} greens_watershed.tif"
        )

    if not os.path.exists(f"greens_watershed_merged_{minsize}.tif"):
        # Merge small regions
        SmallRegionsMerging = otb.Registry.CreateApplication("SmallRegionsMerging")
        SmallRegionsMerging.SetParameterString("inseg", "greens_watershed.tif")
        SmallRegionsMerging.SetParameterString("in", stack_file + "?bands=2")
        SmallRegionsMerging.SetParameterInt("minsize", minsize)
        SmallRegionsMerging.SetParameterString(
            "out", f"greens_watershed_merged_{minsize}.tif?gdal:co:COMPRESS=DEFLATE&gdal:co:PREDICTOR=2"
        )
        SmallRegionsMerging.SetParameterOutputImagePixelType(
            "out", otb.ImagePixelType_uint32
        )
        SmallRegionsMerging.ExecuteAndWriteOutput()

        os.system(
            f"rm -f greens_watershed_merged.tif && ln -s greens_watershed_merged_{minsize}.tif greens_watershed_merged.tif"
        )

    return segmentation_file, stack_file


def greens_remap_labels(indir: str) -> bool:
    """ "Rescale segmentation labels from 0 to maximum"""
    os.chdir(Path(indir) / "green_features")
    path = os.readlink("greens_watershed_merged.tif")
    image = rasterio.open(path)
    profile = image.meta
    if profile["dtype"] != "uint16":
        array = image.read(1)
        if len(np.unique(array)) < 65536:
            new_labels = numba_remap_labels(array.flatten(), array.shape)
            profile.update({"dtype": "uint16", "compress": "deflate", "predictor": 2})
            # Replace link of target with new file
            with rasterio.open(path, "w", **profile) as w:
                w.write_band(1, new_labels)

            return True
    return False


# Greens
def greens_zonal_stats(
    label_image: str,
    stat_image: str,
    overwrite=False,
) -> None:
    """Convert binary image to vector data and write skimage region properties (zonal stats) as attributes"""

    image = rasterio.open(label_image)
    labels = image.read(1)

    df = pd.DataFrame(
        regionprops_table(
            labels,
            properties=[
                "label",
                "area",
                "minor_axis_length",
                "major_axis_length",
            ],
        )
    )
    df["wh_ratio"] = df["minor_axis_length"] / df["major_axis_length"]
    # Fix 1 pix area width / height ratio
    # df['wh_ratio'] = df.wh_ratio.fillna(1)

    for c in ("minor_axis_length", "major_axis_length"):
        df.drop(c, axis=1, inplace=True)
    df.set_index("label", inplace=True)

    # Polygonize
    if not os.path.exists("green_features.gpkg"):
        if os.path.exists("green_features_stats.gpkg") and not overwrite:
            gdf = gpd.read_file("green_features_stats.gpkg")[["fid", "DN"]]
        else:
            os.system(f"gdal_polygonize.py -q {label_image} green_features.gpkg")
            gdf = gpd.read_file("green_features.gpkg")
    else:
        gdf = gpd.read_file("green_features.gpkg")

    gdf.rename({"DN": "label"}, axis=1, inplace=True)
    gdf.set_index("label", inplace=True)
    gdf = gdf[gdf.index > 0].join(df)

    def _rename_column(num, name):
        if name.endswith("_intensity"):
            name = name[:-10]
        return f"{name}_{num}"

    def std(regionmask, intensity):
        return np.std(intensity[regionmask])

    stats = ["label", "mean_intensity"]
    for i, image in enumerate(rasterio.open(stat_image).read()):
        columns = stats + ["max_intensity", "min_intensity"] if i == 0 else stats
        props = pd.DataFrame(
            regionprops_table(
                labels,
                intensity_image=image,
                properties=columns,
                extra_properties=(std,),
            )
        )
        props.set_index("label", inplace=True)
        props.rename(partial(_rename_column, i), axis=1, inplace=True)
        gdf = gdf.join(props)

    gdf.to_file("green_features_stats.gpkg", driver="GPKG")


def greens_gmm(
    data: gpd.GeoDataFrame,
    n_components: int,
    fields: list,
    n_init: int=1,
    seed: int=None,
    bayesian: bool=False,
    outfile:str =""
):
    """Green features clustering with Gaussian Mixture Model"""

    # Remove low intensity big polygons
    filter_index = ~((data["area"] > 2000) & (data["mean_0"] < 0.005))
    df = data[filter_index][fields]
    if bayesian:
        GMM = BayesianGaussianMixture
    else:
        GMM = GaussianMixture

    model = GMM(n_components=n_components, max_iter=200, n_init=n_init, random_state=seed)

    labels = model.fit_predict(df)

    if not model.converged_:
        model = GMM(n_components=n_components, max_iter=500, random_state=seed)
        labels = model.fit_predict(df)
        if not model.converged_:
            raise Exception("GMM can't converge after 500 iterations.")

    df["gmm_label"] = labels

    # Add compactness indices
    #data["schwartzberg"] = data.geometry.apply(s_compactness)
    #data["cvx_hull_score"] = data.geometry.apply(convex_hull_score)

    # Join data with GMM labels and keep more useful columns
    fields_out = [
            "label",
            "area",
            "wh_ratio",
            #"cvx_hull_score",
            "geometry",
            #"schwartzberg"
        ]
    if "mean_0" not in df.columns:
        fields_out.append("mean_0")

    outdata = data[fields_out].join(df)

    if outfile:
        outdata.to_file(outfile, driver="GPKG")

    return outdata


def gmm_bic_aic(
    df, n_components=np.arange(1, 10), seed=None, out_figure="gmm_aic_bic.png"
):
    fig, ax = plt.subplots()
    models = [
        sklearn.mixture.GaussianMixture(
            n_components=n, covariance_type="full", random_state=seed
        ).fit(df)
        for n in n_components
    ]

    ax.plot(n_components, [m.bic(df) for m in models], label="BIC")
    ax.plot(n_components, [m.aic(df) for m in models], label="AIC")
    ax.legend(loc="best")
    plt.xlabel("n_components")
    if out_figure:
        fig.savefig(out_figure, bbox_inches="tight")

    del fig, ax


def greens_classification(
    indir: str,
    classes: int,
    fields: list,
    test_components=0,
    seed=None,
    bayesian=False,
    write_results: bool = True,
    overwrite: bool = False,
):
    """Manage zonal stats and GMM clustering"""

    outdir = str(Path(indir) / "green_features")
    os.chdir(outdir)
    if not os.path.exists("green_features_stats.gpkg") or overwrite:
        greens_zonal_stats(
            "greens_watershed_merged.tif",
            "greens_stack_stats.tif",
            overwrite=overwrite,
        )

    gdf = gpd.read_file("green_features_stats.gpkg")
    fields_str = "_".join(fields)

    if test_components:
        gmm_bic_aic(
            gdf[fields],
            np.arange(1, test_components + 1),
            seed,
            f"green_gmm_test_{fields_str}.png",
        )
        if not classes:
            return None

    outfile = None
    if write_results:
        outfile = f"green_{'bayesian_gmm' if bayesian else 'gmm'}_n_{classes}_{fields_str}.gpkg"

    return greens_gmm(gdf, classes, fields, 1, seed, bayesian, outfile)
