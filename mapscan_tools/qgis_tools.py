
import os
import sys
import numpy as np
from pathlib import Path


def find_qgis():
    """Retourne l'emplacement de QGIS sur la machine"""

    qgs_root = None
    qgs_pyqgis = None

    if sys.platform == 'linux':
        for d in ['/usr', '/usr/local', '/opt/qgis', '/opt/qgis-dev']:
            if Path(d + '/lib/qgis').exists():
                qgs_root = Path(d)
                qgs_pyqgis = qgs_root/'share/qgis/python'

    return str(qgs_root), str(qgs_pyqgis)


HOME = Path.home()
QGS_ROOT, PYQGIS_DIR = find_qgis()
if not QGS_ROOT:
    sys.exit("Can't find QGIS.")
sys.path.append(str(Path(PYQGIS_DIR)/'plugins'))
sys.path.append(PYQGIS_DIR)

from qgis.core import (
    QgsApplication,
    QgsVectorFileWriter,
    QgsVectorLayer,
    QgsProcessingFeedback,
)
from qgis.analysis import QgsNativeAlgorithms
from PyQt5.QtCore import QVariant
import processing
from processing.core.Processing import Processing

# Init QGIS
qgs = QgsApplication([b''], False)
qgs.initQgis()
qgs.setPrefixPath(str(QGS_ROOT))
Processing.initialize()
feedback = QgsProcessingFeedback()


def reproj(file, crs, outdir='memory:'):
    """Reprojection d'une couche"""

    path = str(file)
    name = os.path.basename(path).split('.')[0]
    layer = QgsVectorLayer(path, name)
    params = {
        'INPUT': layer,
        'TARGET_CRS': crs
    }
    if outdir == 'memory:':
            params['OUTPUT'] = outdir + name
    else:
        params['OUTPUT'] = str(outdir/(name + '.shp'))
    res = processing.run('native:reprojectlayer', params, None, feedback)
    del layer

    if isinstance(res['OUTPUT'], QgsVectorLayer):
        return res['OUTPUT']

    return QgsVectorLayer(res['OUTPUT'], name)


def to_gpkg(layer, path):
    """Enregistre un objet QgsVectorLayer sur le disque"""

    writer = QgsVectorFileWriter(
        str(path),
        'utf-8',
        layer.fields(),
        layer.wkbType(),
        layer.sourceCrs(),
        'GPKG'
    )
    writer.addFeatures(layer.getFeatures())
    return True


def extract_blue_lines(indir):
    """Extraction de lignes depuis un scan de cadastre géoréférencé"""

    os.chdir(indir)
    path = "blues_thinned.tif"
    _, basename = os.path.split(path)
    name, ext = os.path.splitext(basename)
    if ext not in ['.tif', '.tiff', '.TIF', '.TIFF']:
        return False

    # Squelette
    params = {'input' : path, 'iterations' : 200, 'output' : 'TEMPORARY_OUTPUT'}
    res = processing.run('grass7:r.thin', params, None, feedback)
    # Vectorisation
    thin = res['output']
    params = {'-b' : False, '-s' : False, '-t' : True, '-v' : False, '-z' : False,
              'output' : 'blue_raw_lines.gpkg', 'GRASS_OUTPUT_TYPE_PARAMETER' : 2, 'input' : thin, 'type' : 0 }
    processing.run('grass7:r.to.vect', params, None, feedback)
    # Nettoyage de la topologie : break, snap, prune
    params = {'-b' : False, '-c' : True, 'GRASS_MIN_AREA_PARAMETER' : 0.0001,
              'GRASS_OUTPUT_TYPE_PARAMETER' : 2, 'GRASS_SNAP_TOLERANCE_PARAMETER' : -1,
              'input' : "blue_raw_lines.gpkg", 'error': 'TEMPORARY_OUTPUT', 'type' : [1], 'output': 'TEMPORARY_OUTPUT',
              'threshold' : '1,30,50,20,1,1,1', 'tool' : [0,1,2,4,6,9,11]}
    res = processing.run('grass7:v.clean', params, None, feedback)
    layer = QgsVectorLayer(res['output'], name='blue_cleaned_lines')
    to_gpkg(layer, 'blue_cleaned_lines.gpkg')
    layer = QgsVectorLayer('blue_cleaned_lines.gpkg', name='blue_lines')
    to_gpkg(layer, '../blue_lines.gpkg')
    # # Explosion des lignes
    # params = {'INPUT': layer, 'OUTPUT':'memory:'}
    # res = processing.run('qgis:explodelines', params, None, feedback)
    # layer = res['OUTPUT']
    # # Calcul de champs
    # layer.addExpressionField('$id', QgsField('gid', QVariant.Int))
    # layer.addExpressionField('$length', QgsField('length', QVariant.Double))
    # # Nettoyage des petits segments
    # params = {
    #     'INPUT': layer,
    #     'EXPRESSION': """ "length" < {} """.format(min_length),
    #     'METHOD': 0
    # }
    # processing.run('qgis:selectbyexpression', params, None, feedback)
    # layer.dataProvider().deleteFeatures([f.id() for f in layer.getSelectedFeatures()])
    # # Export final
    # return to_shp(layer, os.path.join('clean', name + '.shp'))
