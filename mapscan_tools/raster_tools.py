import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
from numba import njit, prange

import rasterio
import rasterio.io
import rasterio.mask
import skimage
import skimage.io
from skimage import color, morphology, filters
from scipy.ndimage import distance_transform_edt
from rasterio.plot import reshape_as_image, reshape_as_raster
import rasterio.crs


def extract_canvas(img_path: str):
    """Extract canvas from raw TIF, required gpkg file with one polygon to frame the map"""
    with rasterio.open(img_path, "r") as src:
        roi = gpd.read_file(img_path.replace(".tif", ".gpkg"))
        extent = roi.geometry
        out_img, out_transform = rasterio.mask.mask(
            src, extent, filled=False, crop=True
        )
        out_meta = src.meta
        out_meta.pop("nodata")

        out_meta.update(
            {
                "driver": "GTiff",
                "dtype": "uint8",
                "height": out_img.shape[1],
                "width": out_img.shape[2],
                "transform": out_transform,
                "tiled": True,
                "compress": "deflate",
                "predictor": 2,
            }
        )

        return out_img.base, out_img.mask, out_meta


def write_color_components(img: np.ndarray, out_meta: dict, outdir: str = "./"):
    """Write separated RGB and HSV components to tiff files"""
    out_meta = out_meta.copy()
    out_meta.update({"count": 3, "dtype": "float32"})

    hsv = color.rgb2hsv(reshape_as_image(img))
    with rasterio.open(outdir + "_hsv.tif", "w", **out_meta) as dest:
        dest.write(reshape_as_raster(hsv).astype(np.float32))

    if img.max() > 1:
        img = (img / img.max()).astype(np.float32)
    with rasterio.open(outdir + "_rgb.tif", "w", **out_meta) as dest:
        dest.write(img)

    gray = color.rgb2gray(reshape_as_image(img))
    if gray.max() > 1:
        gray = gray / gray.max()
    out_meta.update({"count": 1, "dtype": "float32"})
    with rasterio.open(outdir + "_gray.tif", "w", **out_meta) as dest:
        dest.write_band(1, gray)

    return True


### Colors tools
def plot_colors(img: np.ndarray, color_bin: dict, figsize=(50, 60)):
    """Get one plot for each component of color_bin dictionnary"""
    fig, ax = plt.subplots(figsize=figsize)
    skimage.io.imshow(img, interpolation="bilinear")
    colors = {
        "reds": "Reds",
        "greens": "Greens",
        "blues": "Blues",
        "blacks": "gray",
        "whites": "gray",
    }
    for c, array in color_bin.items():
        color = colors[c]
        fig, ax = plt.subplots(figsize=figsize)
        skimage.io.imshow(array, interpolation="bilinear", cmap=color)


def rgb_hist(img, ymax=1e06):
    """RGB histogram"""
    fig, ax = plt.subplots(figsize=(12, 6))
    if img.max() <= 1.0:
        img = (img * 255).astype(np.uint8)

    colors = ("red", "green", "blue")
    channel_ids = (0, 1, 2)

    plt.xlim([0, 256])
    for channel_id, c in zip(channel_ids, colors):
        histogram, bin_edges = np.histogram(img[:, :, channel_id], 256, (0, 256))
        plt.plot(bin_edges[0:-1], histogram, color=c)

    plt.xlabel("Color value")
    plt.ylabel("Pixels")
    plt.ylim(0, ymax)
    plt.show()


def untangle_colors(img: np.ndarray, black_clean: bool = True):
    """From RGB image to binary (Red, Green, Blue, White and Black) components"""
    # assert img.max() <= 1
    hsv = color.rgb2hsv(img)
    r, g, b, wh, bl = (
        red2bin(img, hsv),
        green2bin(img, hsv),
        blue2bin(img, hsv),
        white2bin(hsv),
        black2bin(img, hsv),
    )
    # Optional cleaning using blacks and whites masks
    if black_clean:
        # Dilate by 1px
        bl_dilate = morphology.binary_dilation(bl)
        # bl_dilate = morphology.binary_dilation(bl_dilate)
        # Mask red and green using dilated black mask
        r = r & ~bl_dilate
        g = g & ~bl_dilate
        #b = b & ~bl_dilate

    return r, g, b, wh, bl


# Thresholds
def red2bin(
    img: np.ndarray, hsv: np.ndarray, gweight: float = 1.3, bweight: float = 1.5
) -> np.ndarray:
    """Extract red pixels"""
    r, g, b = img[:, :, 0], img[:, :, 1], img[:, :, 2]
    weighted_threshold = (r > g * gweight) & (r > b * bweight)
    h, _, v = hsv[:, :, 0], hsv[:, :, 1], hsv[:, :, 2]
    hsv_threshold = (h < 0.4) & (v > 0.05) & (v < 0.99)
    return weighted_threshold & hsv_threshold


def green2bin(
    # All
    # img: np.ndarray, hsv: np.ndarray, rweight: float = 1.2, bweight: float = 1.4
    # Boulsa, Djibo, Pama
    # img: np.ndarray, hsv: np.ndarray, rweight: float = 1.0, bweight: float = 1.1
    img: np.ndarray,
    hsv: np.ndarray,
    rweight: float = 1.1,
    bweight: float = 1.2,
) -> np.ndarray:
    """Extract green pixels"""
    r, g, b = img[:, :, 0], img[:, :, 1], img[:, :, 2]
    weighted_threshold = (g > r * rweight) & (g > b * bweight)
    h, _, v = hsv[:, :, 0], hsv[:, :, 1], hsv[:, :, 2]
    # All
    # hsv_threshold = (h > 0.2) & (h < 0.4) & (v > 0.1) & (v < 0.99)
    # Boulsa, Djibo, Pama, Save
    hsv_threshold = (h > 0.2) & (h < 0.4) & (v > 0.2) & (v < 0.98)
    return weighted_threshold & hsv_threshold


def blue2bin(
    img: np.ndarray, hsv: np.ndarray, rweight: float = 1.5, gweight: float = 0.75
) -> np.ndarray:
    """Extract blue pixels"""
    r, g, b = img[:, :, 0], img[:, :, 1], img[:, :, 2]
    weighted_threshold = (b > r * rweight) & (b > g * gweight)
    h, s, v = hsv[:, :, 0], hsv[:, :, 1], hsv[:, :, 2]
    hsv_threshold = (h > 0.3) & (h < 0.7) & (s > 0.3) & (v > 0.1) & (v < 0.95)
    return weighted_threshold & hsv_threshold


def black2bin(img: np.ndarray, hsv: np.ndarray, graymax: float = 0.1) -> np.ndarray:
    """Extract dark pixels"""
    gray = color.rgb2gray(img)
    return (gray < graymax) | (hsv.mean(axis=2) < 0.01)


def white2bin(hsv: np.ndarray, threshold: float = 0.9) -> np.ndarray:
    """Extract bright pixels"""
    _, s, v = hsv[:, :, 0], hsv[:, :, 1], hsv[:, :, 2]
    return (s < 0.1) & (v > threshold)


def mask_array(array, mask, dst_nodata=0, ma=False):
    if ma or dst_nodata is None:
        return np.mask_array.ma(array, mask)
    return np.where(mask, dst_nodata, array)


def write_greens_edt(infile):
    """Euclidian distance transform for green points : can help to classify patterns"""
    img = rasterio.open(infile)
    profile = img.meta
    img = img.read(1)
    dist = distance_transform_edt(np.logical_not(img))
    profile.update(
        {
            "count": 1,
            "dtype": "uint16",
            "compress": "deflate",
            "predictor": 2,
            "nodata": 65535,
        }
    )
    with rasterio.open("greens_edt.tif", "w", **profile) as w:
        w.write(dist.astype(np.uint16), 1)


# Binary morphological operations
def clean_bin_images(colors: dict):
    """Clean bin color images with morphological operations"""
    for key in colors.keys():
        # Small holes
        colors[key] = morphology.remove_small_holes(colors[key], 2, 2)
        if key not in ("whites",):
            colors[key] = morphology.remove_small_holes(colors[key], 4, 2)
        if key in ("blacks", "blues"):
            colors[key] = morphology.remove_small_holes(colors[key], 6, 2)
        # Small objects
        colors[key] = morphology.remove_small_objects(colors[key], 2, 2)
        if key in ("blacks", "blues"):
            colors[key] = morphology.remove_small_objects(colors[key], 4, 2)
        # Dilate
        # if key in ("blues"):
        #    colors[key] = morphology.binary_dilation(colors[key])
        # Opening
        # if key in ("whites"):
        #    colors[key] = morphology.binary_opening(colors[key])
        # Closing
        #if key in ("greens"):
        #   colors[key] = morphology.binary_closing(colors[key])

    return colors


def blue_hydro_network(
    blues: np.ndarray,
    start_min_size: int = 8,
    dilate: int = 1,
    # array([1, 2, 3, 4, 5, 6]), skimage default: array([1, 3, 5, 7, 9])
    sigmas: np.ndarray = np.arange(1, 7),
    threshold: float = 1 / 4,
    #min_blob_size: int = 16,
    min_line_size: int = 4,
) -> tuple:
    """Extract lines using Meijering vesselness index and skeletonization"""
    morphology.remove_small_objects(blues, start_min_size + 1, 2, in_place=True)
    while dilate > 0:
        blues = morphology.binary_dilation(blues)
        dilate -= 1
    # Meijering : vesselness index using successive gaussian filters
    ridges = filters.meijering(blues, sigmas=sigmas, black_ridges=False)
    # Sometimes useful to remove small blobs but it may break more lines
    #ridges_bin = morphology.remove_small_objects(ridges > threshold, min_blob_size + 1, 2)
    ridges_bin = (ridges > threshold)
    # Thinnig, similar to skeletonize, better centrality and nodes preservation
    ridges_bin = morphology.thin(ridges_bin)
    # Remove small lines
    morphology.remove_small_objects(ridges_bin, min_line_size + 1, 2, in_place=True)

    return ridges, ridges_bin


def blue_hydro_zonal(
    blues: np.ndarray,
    closing_radius: int = 5,
    min_hole_size: int = 300,
    opening_radius: int = 2,
    min_blob_size: int = 1000,
) -> np.ndarray:
    """Extract hydro symbol polygons"""

    try:
        selem = morphology.selem.disk
    # skimage >= 0.19
    except AttributeError:
        selem = morphology.footprints.disk
    blues = morphology.binary_closing(blues, selem=selem(closing_radius))
    blues =  morphology.remove_small_holes(blues, min_hole_size + 1)
    blues = morphology.binary_opening(blues, selem=selem(opening_radius))
    blues = morphology.remove_small_objects(blues, min_blob_size + 1)

    return blues


def blue_water_surface(
        blues: np.ndarray,
        opening_radius: int = 3,
        min_blob_size = 500,
        closing_radius: int = 3,
        min_hole_size: int = 200
) -> np.ndarray:

    """Extract water polygons"""
    try:
        selem = morphology.selem.disk
    # skimage >= 0.19
    except AttributeError:
        selem = morphology.footprints.disk
    blues = morphology.binary_opening(blues, selem=selem(opening_radius))
    blues = morphology.remove_small_objects(blues, min_blob_size + 1)
    blues = morphology.binary_closing(blues, selem=selem(closing_radius))
    blues =  morphology.remove_small_holes(blues, min_hole_size + 1)

    return blues


# Optimization
@njit
def numba_remap_labels(array_flat: np.ndarray, outshape: tuple) -> np.ndarray:
    """Remap labels, from 0 to max - must be less than 65535 different labels"""

    keys = np.unique(array_flat)
    values = np.arange(len(keys))
    assert values.max() < 65536

    ind_sort = np.argsort(keys)
    keys_sorted = keys[ind_sort]
    values_sorted = values[ind_sort]
    s_keys = set(keys)

    for j in prange(array_flat.shape[0]):
        if array_flat[j] in s_keys:
            ind = np.searchsorted(keys_sorted, array_flat[j])
            array_flat[j] = values_sorted[ind]

    output = np.reshape(array_flat, outshape).astype(np.uint16)
    return output
